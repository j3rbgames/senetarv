﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DentedPixel;

public class controller : MonoBehaviour
{
    public GameObject fondo;
    public GameObject papiro;


    public Button botonDerecha;
    public Button botonIzquierda;

    int contadorDerecha = 0;
    int contadorIzquierda = 0;


    // Start is called before the first frame update
    void Start()
    {
        botonDerecha.onClick.AddListener(movimientoGatoDerecha);
        botonIzquierda.onClick.AddListener(movimientoGatoIzquierda);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void movimientoGatoDerecha()
    {
        Debug.Log("Contador Derecha" + contadorDerecha);
        Debug.Log("Contador Izquierda" + contadorIzquierda);

        if (!LeanTween.isTweening(papiro) && contadorDerecha < 2)
        {
            botonIzquierda.gameObject.SetActive(true);
            LeanTween.moveX(papiro, papiro.transform.position.x + 40f, .5f);
            LeanTween.moveX(fondo, fondo.transform.position.x + 5f, 0.5f);
            contadorDerecha++;
            contadorIzquierda--;
            if (contadorDerecha > 1)
            {
                botonDerecha.gameObject.SetActive(false);
            }
        }
        
    }

    void movimientoGatoIzquierda()
    {
        Debug.Log("Contador Derecha" + contadorDerecha);
        Debug.Log("Contador Izquierda" + contadorIzquierda);

        if (!LeanTween.isTweening(papiro) && contadorIzquierda < 2)
        {
            botonDerecha.gameObject.SetActive(true);
            LeanTween.moveX(papiro, papiro.transform.position.x - 40f, .5f);
            LeanTween.moveX(fondo, fondo.transform.position.x - 5f, 0.5f);
            contadorDerecha--;
            contadorIzquierda++;
            if (contadorIzquierda > 1)
            {
                botonIzquierda.gameObject.SetActive(false);
            }
        }
        
    }

}
