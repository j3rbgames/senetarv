﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersonajesMenuController : MonoBehaviour
{
    private bool isNormal = true;
    public Button botonDerecha;
    public Button botonIzquierda;
    public GameObject personaje1;
    public GameObject textoPersonaje1;
    public GameObject personaje2;
    public GameObject textoPersonaje2;

    private void Awake()
    {
        botonDerecha.onClick.AddListener(RotarDerecha);
        botonIzquierda.onClick.AddListener(RotarIzquierda);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void RotarDerecha()
    {
        Debug.Log("MenuPersonaje: rotando a la derecha. Normal: " + isNormal);
        if (!LeanTween.isTweening(personaje1) && !LeanTween.isTweening(personaje2))
        {
            if (isNormal)
            {
                LeanTween.rotateY(personaje1, 0f, .4f).setEase(LeanTweenType.easeInQuad);
                LeanTween.rotateY(personaje2, 0f, .4f).setEase(LeanTweenType.easeOutQuad);
            }
            else
            {
                LeanTween.rotateY(personaje1, 180f, .4f).setEase(LeanTweenType.easeOutQuad);
                LeanTween.rotateY(personaje2, 180f, .4f).setEase(LeanTweenType.easeInQuad);
            }
            AlternarTextos();
            isNormal = !isNormal;
        }
    }

    void RotarIzquierda()
    {
        Debug.Log("MenuPersonaje: rotando a la izquierda. Normal: " + isNormal);
        Debug.Log("MenuPersonaje: Personaje 1: Inicio = " + personaje1.transform.rotation.eulerAngles.y);
        Debug.Log("MenuPersonaje: Personaje 2: Inicio = " + personaje2.transform.rotation.eulerAngles.y);
        if (!LeanTween.isTweening(personaje1) && !LeanTween.isTweening(personaje2))
        {
            if (isNormal)
            {
                LeanTween.rotateY(personaje1, 360f, .4f).setEase(LeanTweenType.easeInQuad);
                LeanTween.rotateY(personaje2, 360f, .4f).setEase(LeanTweenType.easeOutQuad);
            }
            else
            {
                LeanTween.rotateY(personaje1, 180f, .4f).setEase(LeanTweenType.easeOutQuad);
                LeanTween.rotateY(personaje2, 180f, .4f).setEase(LeanTweenType.easeInQuad);
            }
            AlternarTextos();
            isNormal = !isNormal;
        }
    }

    void AlternarTextos()
    {
        if (isNormal)
        {
            LeanTween.alpha(textoPersonaje1.GetComponent<RectTransform>(), 0f, .1f);
            LeanTween.alpha(textoPersonaje2.GetComponent<RectTransform>(), 255f, .2f).setDelay(.2f);
        }
        else
        {
            LeanTween.alpha(textoPersonaje2.GetComponent<RectTransform>(), 0f, .1f);
            LeanTween.alpha(textoPersonaje1.GetComponent<RectTransform>(), 255f, .2f).setDelay(.2f);
        }
    }
}
