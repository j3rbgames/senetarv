﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class video : MonoBehaviour
{
    public UnityEngine.Video.VideoPlayer ourVideo;
    public GameObject iconPlay;
    public GameObject buttonBack;

    void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnMouseDown()
    {
        vClick();
    }

    public void vClick()
    {
        if (ourVideo.isPlaying)
        {
            iconPlay.gameObject.SetActive(true);
            buttonBack.gameObject.SetActive(true);
            ourVideo.Pause();
        }
        else
        {
            iconPlay.gameObject.SetActive(false);
            buttonBack.gameObject.SetActive(true);
            ourVideo.Play();
        }
    }

    public void volver()
    {
        SceneManager.LoadScene("MenuSwipe");
    }
}
