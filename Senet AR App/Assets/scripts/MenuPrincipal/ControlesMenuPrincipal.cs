﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlesMenuPrincipal : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void abrirJugar()
    {
        SceneManager.LoadScene("GroundPlaneTestScene");
    }

    public void abrirEscenaComic()
    {
        SceneManager.LoadScene("comic");
    }

    public void abrirEscenaVideo()
    {
        SceneManager.LoadScene("video");
    }

    public void abrirEscenaPersonajes()
    {
        SceneManager.LoadScene("Menu Personajes");
    }

    public void abrirEscenaInstrucciones()
    {
        SceneManager.LoadScene("instrucciones");
    }
}
